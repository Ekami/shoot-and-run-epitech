#-------------------------------------------------
#
# Project created by QtCreator 2013-06-16T12:22:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Rush
TEMPLATE = app
QMAKE_CXXFLAGS += -g
INCLUDES  = includes
INCLUDEPATH += $$INCLUDES
INCLUDEPATH += $$INCLUDES/editor
INCLUDEPATH += $$INCLUDES/maingame
LIBS += -Llibs/linux/ -lsfml-audio -lsfml-graphics -lsfml-system -lsfml-window -l:libGLEW.so.1.9 -l:libjpeg.so.8 -Wl,-rpath=libs/linux

SOURCES +=  src/main.cpp\
                            src/Menu.cpp \
                            src/Player.cpp \
                            src/TileMaper.cpp   \
                            src/Exception.cpp   \
                            src/Utils.cpp \
                            src/Unit.cpp \
                            src/Berserk.cpp \
                            src/Tank.cpp \
                            src/MissileLauncher.cpp \
                            src/Team.cpp \
                            src/QsfmlCanvas.cpp \
                            src/editor/MapCreatorMW.cpp \
                            src/editor/CreatorCanvas.cpp \
                            src/maingame/MainWindow.cpp \
                            src/maingame/MapCanvas.cpp \
                            src/maingame/MekaEngine.cpp

HEADERS  += 	$$INCLUDES/editor/CreatorCanvas.hh \
                $$INCLUDES/editor/MapCreatorMW.hh \
                $$INCLUDES/maingame/MainWindow.hh \
                $$INCLUDES/maingame/MapCanvas.hh \
                $$INCLUDES/maingame/MekaEngine.hh \
                $$INCLUDES/maingame/MekaMovesException.hh \
                $$INCLUDES/Berserk.hh \
                $$INCLUDES/Team.hh \
                $$INCLUDES/Exception.hh \
                $$INCLUDES/Menu.hh \
                $$INCLUDES/MissileLauncher.hh \
                $$INCLUDES/Player.hh \
                $$INCLUDES/QsfmlCanvas.hh \
                $$INCLUDES/Tank.hh \
                $$INCLUDES/TileMaper.hh \
                $$INCLUDES/Unit.hh \
                $$INCLUDES/Utils.hh

FORMS    +=     ui/Menu.ui \
                ui/mainwindow.ui \
                ui/Mapcreatormw.ui

RESOURCES += resources/pictures.qrc
