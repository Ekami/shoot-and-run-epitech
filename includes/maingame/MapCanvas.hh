/* 
 * File:   MapCanvas.hh
 * Author: godard_b
 *
 * Created on July 21, 2013, 7:26 PM
 */

#ifndef MAPCANVAS_HH
#define	MAPCANVAS_HH
#include <iostream>
#include <string>
#include <vector>
#include <QApplication>
#include <QFrame>
#include "QsfmlCanvas.hh"
#include "Player.hh"
#include "TileMaper.hh"
#include "Exception.hh"
#include "Utils.hh"
#include "MapParserException.hpp"
#include "MekaEngine.hh"
#include "MainWindow.hh"
#include "MapParserException.hpp"

class MainWindow;

class MapCanvas : public QSFMLCanvas
{

    Q_OBJECT
  public:

    enum  UnitAction
    {
        NOTHING = 101,
        MOVE = 102,
        ATTACK = 103,
        ACTIVATE_SPECIAL = 104
    } ;
    //Upper value slow down the framerate
    MapCanvas(QWidget* parent, const QPoint& position, const QSize& size, const std::vector<int> boardMapVector);
    ~MapCanvas();
    static const int              FRAME_TIME = 10;
    void        mousePressEvent(QMouseEvent *ev);
    int         getCaseSize() const;
    int         getCasesCountX() const;
    int         getCasesCountY() const;
    int         getFrameTime() const;
    int         getMapSizeX() const;
    int         getMapSizeY() const;
    Unit::FieldType     getFieldSelected() const;
    Unit                *getLastUnitSelected();
    void        setCasesCountX(const int casesCountX);
    void        setCasesCountY(const int casesCountY);
    void        setFrameTime(const int frameTime);
    void        setFieldSelected(int fieldSelected);
    int         *getBoardArray() const;
    void        setBoardArray(const std::vector<int> &data);
    int         getBoardArrayValue(const int x, const int y) const;
    void        setBoardArrayValue(const int x, const int y, const int value);
    void        getBoardArrayIndexFromMouse(const int x, const int y, int &retX, int &retY);
    void        setLastUnitAction(const UnitAction action);
    bool        lastUnitIsFromTeam(const int fromTeam);
    void        resetUnitsTurnsLimitations();
signals:
    void        updateFieldInfosSignal(int fieldSelected);
    void        updateMekaInfosSignal(int lifePoints, int attack, int speed, int scope);
    void        badMekaMove(const std::string &error);
    void        info(const std::string &error);
    void        activateMoveButton(const bool activated);
    void        activateAttackButton(const bool activated);
    void        activateSpecialButton(const bool activated);
    void        activateCancelActionButton(const bool activated);
    void        teamWins(const int teamWinner);
  private:
    void        handleLastUnitActions(const int onPosX, const int onPosY);
    void        onInit();
    void        onUpdate();

    int              casesCountX, casesCountY;
    int              caseSize;
    int              mapSizeX, mapSizeY;
    int              hoverX, hoverY;
    int              fieldSelected;
    int              *boardArray;
    Unit             *lastUnitSelected;
    UnitAction       lastUnitAction;
    MekaEngine       *mekaEngine;
    TileMaper        boardMap;
    sf::Text         caseValue;
    sf::Font         font;
} ;

#endif	/* MAPCANVAS_HH */

