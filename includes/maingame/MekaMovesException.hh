/* 
 * File:   MekaMovesException.hh
 * Author: godard_b
 *
 * Created on July 27, 2013, 12:49 AM
 */

#ifndef MEKAMOVESEXCEPTION_HH
#define	MEKAMOVESEXCEPTION_HH
#include <iostream>
#include <string>
#include <exception>

namespace MekaMovesException
{

    class MekaMovesException : public std::exception
    {
      protected:
        std::string msg;
      public:

        virtual ~MekaMovesException(void) throw () { }

        virtual const char* what()
        {
            return msg.c_str();
        }
    } ;

    /*
     * The meka cannot move to the position
     */
    class BadMoveException : public MekaMovesException
    {
      public:

        BadMoveException(const std::string m = "You cannot move to this location")
        {
            msg = m;
        }

        virtual ~BadMoveException(void) throw () { }
    } ;

    /*
     * The meka has already moved for the current turn
     */
    class AlreadyMoveException : public MekaMovesException
    {
      public:

        AlreadyMoveException(const std::string m = "This unit has already moved for this turn")
        {
            msg = m;
        }

        virtual ~AlreadyMoveException(void) throw () { }
    } ;

    /*
     * The meka has already launched an attack for the current turn
     */
    class AlreadyAttackException : public MekaMovesException
    {
      public:

        AlreadyAttackException(const std::string m = "This unit has already attacked for this turn")
        {
            msg = m;
        }

        virtual ~AlreadyAttackException(void) throw () { }
    } ;

    /*
     * The meka move is too far
     */
    class FarMoveException : public MekaMovesException
    {
      public:

        FarMoveException(const std::string m = "You cannot move that far")
        {
            msg = m;
        }

        virtual ~FarMoveException(void) throw () { }
    } ;

    /*
     * The meka attack is out of scope
     */
    class AttackOutOfScopeException : public MekaMovesException
    {
      public:

        AttackOutOfScopeException(const std::string m = "Your attack is out of scope")
        {
            msg = m;
        }

        virtual ~AttackOutOfScopeException(void) throw () { }
    } ;

    /*
     * The meka attack has no target
     */
    class NoTargetFoundException : public MekaMovesException
    {
      public:

        NoTargetFoundException(const std::string m = "No target found at this position")
        {
            msg = m;
        }

        virtual ~NoTargetFoundException(void) throw () { }
    } ;
} //namespace MekaMovesException

#endif	/* MEKAMOVESEXCEPTION_HH */

