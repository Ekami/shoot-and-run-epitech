/* 
 * File:   MapParserException.hpp
 * Author: godard_b
 *
 * Created on July 25, 2013, 12:44 AM
 */

#ifndef MAPPARSEREXCEPTION_HPP
#define	MAPPARSEREXCEPTION_HPP
#include <iostream>
#include <string>
#include <exception>

namespace MapParserException
{

    class MapParserException : public std::exception
    {
      protected:
        std::string msg;
      public:

        virtual ~MapParserException(void) throw () { }

        virtual const char* what()
        {
            return msg.c_str();
        }
    } ;

    /*
     * The file contains corrupt data
     */
    class BadData : public MapParserException
    {
      public:

        BadData(const std::string m = "Error: The content of the file could not be read!")
        {
            msg = m;
        }

        virtual ~BadData(void) throw () { }
    } ;

    /*
     * The file is not valid
     */
    class InvalidFile : public MapParserException
    {
      public:

        InvalidFile(const std::string m = "Error: The map file is invalid!")
        {
            msg = m;
        }

        virtual ~InvalidFile(void) throw () { }
    } ;

} //namespace MapParserException

#endif	/* MAPPARSEREXCEPTION_HPP */

