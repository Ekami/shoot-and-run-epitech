/* 
 * File:   MekaEngine.hh
 * Author: godard_b
 *
 * Created on July 26, 2013, 2:27 AM
 */

#ifndef MEKAENGINE_HH
#define	MEKAENGINE_HH
#include <iostream>
#include <string>
#include "TileMaper.hh"
#include "Exception.hh"
#include "MekaMovesException.hh"
#include "Team.hh"

class MapCanvas;

class MekaEngine
{
  public:

    enum UnitType
    {
        TANK_BLUE = 0,
        MISSILE_LAUNCHER_BLUE = 1,
        BERSERK_BLUE = 2,
        TANK_RED = 3,
        MISSILE_LAUNCHER_RED = 4,
        BERSERK_RED = 5,
        NONE = 6
    } ;

    enum ArrowType
    {
        ARROW_DOWN1 = 0,
        ARROW_DOWN2 = 1,
        ARROW_DOWN3 = 2,
        ARROW_UP1 = 3,
        ARROW_UP2 = 4,
        ARROW_UP3 = 5,
        ARROW_NOTHING = 6,
    } ;
    MekaEngine(MapCanvas &parent, const int mekaMapX, const int mekaMapY, const int mekaCaseSize);
    ~MekaEngine();
    void            clearMapArray();
    void            setMekaArrayValue(const int x, const int y, const int value);
    void            updateArrowPosition(const int onPosX, const int onPosY);
    TileMaper       getArrowTileMap();
    TileMaper       getTileMap();
    Unit            *getUnitAt(const int x, const int y);
    bool            isUnitFromTeam(Unit *unit, const int fromTeam);
    void            removeUnit(Unit *unit);
    void            resetUnitsTurnsLimitations();

    //Throws MekaMovesException
    void            moveUnitToPosition(Unit *unit, const int xPos, const int yPos);
    int             attackUnitAtPosition(Unit *unit, const int xPos, const int yPos);
    int             checkEndGame();

    //Used to test whether attack scope is sufficient of if a unit can move to the given location
    bool            posIsInRange(const int originX, const int originY, const int toX, const int toY, const int range);

  private:
    void             initTeams();

    MapCanvas        &parent;
    int              mekaArraySize;
    int              *mekaArray;
    int              mekaMapX, mekaMapY;
    int              mekaCaseSize;
    TileMaper        mekaMap;
    TileMaper        arrowMap;
    Team             teams[2];
} ;

#endif	/* MEKAENGINE_HH */

