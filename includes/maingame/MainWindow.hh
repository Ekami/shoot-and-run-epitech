#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <iostream>
#include <fstream>
#include <QApplication>
#include <QMainWindow>

#include "MapCanvas.hh"

class Menu;
class MapQFrame;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    MainWindow(Menu *parent, const std::string &mapFile);
    Menu    &getParent() const;
    ~MainWindow();
    Ui::MainWindow *ui;
  private slots:
    void        onUpdateFieldInfos(int fieldSelected);
    void        onUpdateMekaInfos(int lifePoints, int attack, int speed, int scope);
    void        onSwitchTeamTurn(int team);
    void        onAddInfo(const std::string &str);
    void        onActivateMoveButton(const bool isActivated);
    void        onActivateAttackButton(const bool isActivated);
    void        onActivateSpecialButton(const bool isActivated);
    void        onActivateCancelActionButton(const bool isActivated);
    void        onTeamWins(const int teamWinner);
    void        on_btMoveUnit_clicked();
    void        on_btActivateSpecial_clicked();
    void        on_btAttackUnit_clicked();
    void        on_btCancelAction_clicked();
    void        on_btFinishTurn_clicked();
    void        on_actionQuit_triggered();
  private:
    void                    connectCanvasSignals();
    std::vector<int>        parseMapFile(const std::string &mapFile);
    void                    closeEvent(QCloseEvent *);

  private:
    Menu      *parent;
    QFrame    *frame;
    MapCanvas *canvas;
    bool      isUnitSelected;
    int       curTeam;
} ;

#endif // MAINWINDOW_HH
