#ifndef MYQFRAME_HH
#define MYQFRAME_HH

#include <QFrame>
#include <QMouseEvent>
#include <QEvent>
#include <QDebug>
#include "MapCanvas.hh"
#include "MainWindow.hh"

class MapQFrame : public QFrame
{
    Q_OBJECT
  public:
    explicit MapQFrame(MainWindow *parent);
    void mousePressEvent(QMouseEvent *ev);
    void keyPressEvent(QKeyEvent *ev);
    void Mouse_Pressed();
    std::string qgetInfo() const;
signals:
    void addinfo();

  private:
    int         x, y;
    int         turns;
    char        playerId;
    std::string       info;
    MainWindow    *parent;
    MapCanvas      *map;
} ;

#endif // MYQFRAME_HH
