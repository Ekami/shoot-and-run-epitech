/* 
 * File:   Unit.hh
 * Author: godard_b
 *
 * Created on July 23, 2013, 11:44 AM
 */

#ifndef UNIT_HH
#define	UNIT_HH

class Unit
{
  public:

    enum FieldType
    {
        MOUTAINS = 0,
        RIVER = 1,
        PLAIN = 2
    } ;

    enum UnitType
    {
        TANK = 101,
        BERSERK = 102,
        MISSILE_LAUNCHER = 103
    } ;
    Unit();
    virtual ~Unit();
    int         getX() const;
    int         getY() const;
    void        setPosX(const int x);
    void        setPosY(const int y);
    void        setScope(const int scope);
    int         getScope() const;
    void        setAttack(const int attack);
    int         getAttack() const;
    void        setSpeed(const int speed);
    int         getSpeed() const;
    int         getLifePoints() const;
    void        setLifePoint(const int lifePoints);
    FieldType   getOnFieldType() const;
    void        setOnFieldType(const FieldType type);
    bool        getCanMove() const;
    void        setCanMove(const bool canMove);
    bool        getCanAttack() const;
    void        setCanAttack(const bool canMove);
    virtual void        enableSpecial() = 0;
    virtual void        disableSpecial() = 0;
    virtual bool        isSpecialActive() = 0;
    virtual UnitType    getType() = 0;
  private:
    int         x, y;
    int         scope;
    int         attack;
    int         speed;
    int         lifePoints;
    bool        canMove;
    bool        canAttack;
    FieldType   onFieldType;
} ;

#endif	/* UNIT_HH */

