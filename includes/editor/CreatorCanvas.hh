/* 
 * File:   CreatorCanvas.hh
 * Author: godard_b
 *
 * Created on July 21, 2013, 7:26 PM
 */

#ifndef CREATORCANVAS_HH
#define	CREATORCANVAS_HH
#include <iostream>
#include <string>
#include <QApplication>
#include "QsfmlCanvas.hh"
#include "Player.hh"
#include "TileMaper.hh"
#include "Exception.hh"
#include "Utils.hh"

class MapCreatorMW;

class CreatorCanvas : public QSFMLCanvas
{
    Q_OBJECT
          public:
            //Upper value slow down the framerate
            static const int              FRAME_TIME = 100;
    CreatorCanvas(QWidget* Parent, const QPoint& Position, const QSize& Size);
    ~CreatorCanvas();
    void    mousePressEvent(QMouseEvent *ev);
    void    mouseMoveEvent(QMouseEvent* ev);

    int getCaseSize() const;
    int getCasesCountX() const;
    int getCasesCountY() const;
    int getFrameTime() const;
    int getMapSizeX() const;
    int getMapSizeY() const;
    int getFieldSelected() const;
    void setCasesCountX(const int casesCountX);
    void setCasesCountY(const int casesCountY);
    void setFrameTime(const int frameTime);
    void setFieldSelected(int fieldSelected);
    int  *getBoardArray() const;
    void clearMap();

  private:
    void        setBoardArrayValue(const int x, const int y, const int value);
    void        getBoardArrayIndexFromMouse(const int x, const int y, int &retX, int &retY);
    void        onUpdate();
    void        onInit();

    int              casesCountX, casesCountY;
    int              caseSize;
    int              mapSizeX, mapSizeY;
    int              curPosX, curPosY;
    int              fieldSelected;
    sf::Text         caseValue;
    sf::Font         font;
    int              *boardArray;
    TileMaper        boardMap;
} ;

#endif	/* CREATORCANVAS_HH */

