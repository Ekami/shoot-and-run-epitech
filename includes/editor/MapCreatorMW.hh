#ifndef MAPCREATORMW_HH
#define MAPCREATORMW_HH

#include <QMainWindow>
#include <QFrame>
#include <fstream>
#include "CreatorCanvas.hh"

namespace Ui
{
    class MapCreatorMW;
}

class MapCreatorMW : public QMainWindow
{

    Q_OBJECT
  public:
    enum FieldType
    {
        MOUTAINS = 0,
        WATER = 1,
        PLAIN = 2
    } ;
    explicit MapCreatorMW(QWidget *parent = 0);
    ~MapCreatorMW();
  private:
    void        createMapFile(const std::string &mapFilePath);
  private slots:
    void on_btPickMoutain_clicked();
    void on_btPickWater_clicked();
    void on_btPickPlain_clicked();
    void on_sbMapWidth_valueChanged(int arg1);
    void on_sbMapHeight_valueChanged(int arg1);
    void on_pushButton_clicked();
    void on_btSaveMap_clicked();
    void closeEvent(QCloseEvent *);

  private:
    Ui::MapCreatorMW *ui;
    QFrame           *frame;
    CreatorCanvas    *canvas;
    QWidget          *parent;
} ;

#endif // MAPCREATORMW_HH
