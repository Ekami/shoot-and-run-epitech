#ifndef TILEMAP_HH
#define	TILEMAP_HH

#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include "TileMaper.hh"

class TileMaper : public sf::Drawable, public sf::Transformable
{
  public:
    TileMaper(const std::string& tileset);
    bool load(sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height);

  private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    sf::VertexArray          m_vertices;
    sf::Texture                m_tileset;
} ;


#endif	/* TILEMAP_HH */

