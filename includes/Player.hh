/* 
 * File:   Player.hh
 * Author: godard_b
 *
 * Created on June 18, 2013, 1:20 AM
 */

#ifndef PLAYER_HH
#define	PLAYER_HH
#include <iostream>
#include <string>

class Player {
public:
    int getScore() const;
    void setScore(const int score);
private:
    int score;
};

#endif	/* PLAYER_HH */

