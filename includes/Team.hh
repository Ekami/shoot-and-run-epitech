/* 
 * File:   Team.hh
 * Author: godard_b
 *
 * Created on July 26, 2013, 4:48 AM
 */

#ifndef TEAM_HH
#define	TEAM_HH
#include <iostream>
#include <string>
#include <vector>
#include "Unit.hh"
#include "Tank.hh"
#include "Berserk.hh"
#include "MissileLauncher.hh"

class Team
{
  public:
    Team();
    ~Team();
    void                addUnit(Unit *unit);
    void                removeUnit(Unit *unit);
    std::vector<Unit *> getUnits() const;
  private:
    std::vector<Unit *>   units;
} ;

#endif	/* TEAM_HH */

