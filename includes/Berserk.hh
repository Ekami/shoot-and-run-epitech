/* 
 * File:   Berserk.hh
 * Author: godard_b
 *
 * Created on July 23, 2013, 3:33 PM
 */

#ifndef BERSERK_HH
#define	BERSERK_HH
#include <iostream>
#include <string>
#include "Unit.hh"

class Berserk : public Unit
{
  public:
    Berserk(const int x = 0, const int y = 0, const Unit::FieldType type = Unit::PLAIN);
    void        enableSpecial();
    void        disableSpecial();

    /*
     * Se déplace en sautant, ignorant les unités sur son chemin
     */
    bool        isSpecialActive();
    UnitType    getType();
  private:
    bool        specialActive;
} ;

#endif	/* BERSERK_HH */

