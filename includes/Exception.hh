#ifndef		EXCEPTION_HH_
#define	EXCEPTION_HH_

#include	<exception>
#include	<string>
#include	<iostream>

class		GlobalException : public std::exception
{
  protected:
    std::string	_errormsg;
  public:
    GlobalException(const std::string &error);
    ~GlobalException() throw ();
    const char*	what() const throw ();
} ;

#endif		/* !EXCEPTION_HH_ */
