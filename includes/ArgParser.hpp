/* 
 * File:   ArgParser.hpp
 * Author: godard_b
 *
 * Created on July 24, 2013, 6:51 AM
 */

#ifndef ARGPARSER_HPP
#define	ARGPARSER_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>
#include "Utils.hh"

template <typename T>
class ArgParser
{
  public:

    static T  getParameter(const std::string &cmdMsg, const unsigned int paramNb)
    {
        T                                               elem;
        std::vector<std::string>      params;

        split(cmdMsg, ' ', params);
        if (paramNb > params.size() - 1)
            return ;
        return reinterpret_cast<T> (elem[paramNb + 1]);
    }
} ;

template <>
class ArgParser<int>
{
  public:

    static int  getParameter(const std::string &cmdMsg, const int unsigned paramNb)
    {
        int                                               elem = 0;
        std::vector<std::string>        params;

        if (cmdMsg.empty())
            return -1;
        split(cmdMsg, ' ', params);
        if (paramNb > params.size() - 1)
            return -1;
        std::istringstream buffer(params[paramNb]);

        buffer >> elem;
        return elem;
    }
} ;

template <>
class ArgParser<std::string>
{
  public:

    static std::string  getParameter(const std::string &cmdMsg, const unsigned int paramNb, const char delim)
    {
        std::vector<std::string>        params;

        if (cmdMsg.empty())
            return std::string();
        split(cmdMsg, delim, params);
        if (paramNb > params.size() - 1)
            return 0;
        return params[paramNb];
    }
} ;

#endif	/* ARGPARSER_HPP */

