/* 
 * File:   Utils.hh
 * Author: godard_b
 *
 * Created on July 11, 2013, 8:23 PM
 */

#ifndef UTILS_HH
#define	UTILS_HH
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "MapParserException.hpp"

namespace Utils
{
    std::string toStr(const int nbr);
    int         toInt(const std::string & nb);
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
} //namespace Utils

#endif	/* UTILS_HH */

