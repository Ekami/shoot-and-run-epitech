/* 
 * File:   Tank.hh
 * Author: godard_b
 *
 * Created on July 23, 2013, 3:35 PM
 */

#ifndef TANK_HH
#define	TANK_HH
#include <iostream>
#include <string>
#include "Unit.hh"

class Tank : public Unit
{
  public:
    Tank(const int x = 0, const int y = 0, const Unit::FieldType type = Unit::PLAIN);
    void        disableSpecial();
    void        enableSpecial();

    /*
     * Double les dégats sur la prochaine attaque
     */
    bool        isSpecialActive();
    UnitType    getType();
  private:
    bool        specialActive;
} ;

#endif	/* TANK_HH */

