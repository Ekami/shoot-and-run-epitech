/* 
 * File:   MissileLauncher.hh
 * Author: godard_b
 *
 * Created on July 23, 2013, 3:36 PM
 */

#ifndef MISSILELAUNCHER_HH
#define	MISSILELAUNCHER_HH
#include <iostream>
#include <string>
#include "Unit.hh"

class MissileLauncher : public Unit
{
  public:
    MissileLauncher(const int x = 0, const int y = 0, const Unit::FieldType type = Unit::PLAIN);
    void        enableSpecial();
    void        disableSpecial();

    /*
     * La prochaine attaque touche également les 8 cases autour avec une
     * attaque de puissance 3. (Ne varie pas en fonction du terrain)
     */
    bool        isSpecialActive();
    UnitType    getType();
  private:
    bool        specialActive;
} ;

#endif	/* MISSILELAUNCHER_HH */

