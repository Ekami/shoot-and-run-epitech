#ifndef MENU_HH
#define MENU_HH

#include <QDialog>
#include <QString>
#include "MainWindow.hh"
#include "MapCanvas.hh"
#include "QsfmlCanvas.hh"
#include "MapCreatorMW.hh"
#include "MapParserException.hpp"

namespace Ui
{
    class Menu;
}

class Menu : public QDialog
{
    Q_OBJECT

  public:
    explicit Menu(QWidget *parent = 0);
    ~Menu();

    MapCanvas* getMapView() const;

  private slots:
    void on_btStartGame_clicked();
    void on_btNewMap_clicked();
    void on_btSearchFile_clicked();
    void on_btQuit_clicked();

  private:
    QWidget        &parent;
    Ui::Menu       *ui;
    MapCanvas      *mapView;
    MainWindow     *mainWindow;
    MapCreatorMW   *creator;
    std::string    mapFile;
} ;

#endif // MENU_HH
