/* 
 * File:   Berserk.cpp
 * Author: godard_b
 * 
 * Created on July 23, 2013, 3:33 PM
 */

#include "Berserk.hh"

Berserk::Berserk(const int x, const int y, const Unit::FieldType type)
{
    this->setPosX(x);
    this->setPosY(y);
    this->setLifePoint(15);
    this->setAttack(4);
    this->setSpeed(5);
    this->setScope(1);
    this->setOnFieldType(type);
}

void        Berserk::enableSpecial()
{
    this->specialActive = true;
}

void        Berserk::disableSpecial()
{
    this->specialActive = false;
}

bool        Berserk::isSpecialActive()
{
    return specialActive;
}

Unit::UnitType    Berserk::getType()
{
    return Unit::BERSERK;
}