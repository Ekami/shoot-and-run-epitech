/* 
 * File:   Team.cpp
 * Author: godard_b
 * 
 * Created on July 26, 2013, 4:48 AM
 */

#include "Team.hh"

Team::Team() { }

void    Team::addUnit(Unit *unit)
{
    this->units.push_back(unit);
}

void    Team::removeUnit(Unit *unit)
{
    for (std::vector<Unit *>::iterator it = units.begin(); it != units.end(); ++it)
    {
        if (unit == (*it))
        {
            units.erase(it);
            delete unit;
            break;
        }

    }
}

std::vector<Unit *> Team::getUnits() const
{
    return units;
}

Team::~Team()
{
    for (unsigned int i = 0; i < units.size(); i++)
        delete units[i];
    units.clear();
}