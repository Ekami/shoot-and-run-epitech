#include "Menu.hh"
#include "ui_Menu.h"
#include "MapCreatorMW.hh"

Menu::Menu(QWidget *parent) :
parent(*parent),
ui(new Ui::Menu),
creator(new MapCreatorMW(this))
{
    ui->setupUi(this);
}

Menu::~Menu()
{
    delete ui;
    delete creator;
    if (mainWindow != NULL)
        delete mainWindow;
}

MapCanvas* Menu::getMapView() const
{
    return mapView;
}

void Menu::on_btStartGame_clicked()
{
    try
    {
        mainWindow = new MainWindow(this, ui->leFile->text().toStdString());
        this->hide();
        mainWindow->show();
    }
    catch (MapParserException::MapParserException &e)
    {
        QMessageBox::warning(this, "Error", e.what());
        delete mainWindow;
    }
}

void Menu::on_btNewMap_clicked()
{
    this->hide();
    creator->show();
}

void Menu::on_btSearchFile_clicked()
{
    QString file = QFileDialog::getOpenFileName(this, "Select a map file", ".");

    mapFile = file.toStdString();
    ui->leFile->setText(file);
}

void Menu::on_btQuit_clicked()
{
    exit(0);
}
