/* 
 * File:   MapCanvas.cpp
 * Author: godard_b
 * 
 * Created on July 21, 2013, 7:26 PM
 */

#include "MapCanvas.hh"
#include <sstream>

MapCanvas::MapCanvas(QWidget* parent, const QPoint& position, const QSize& size, const std::vector<int> boardMapVector)
: QSFMLCanvas(parent, position, size, FRAME_TIME),
casesCountX(31),
casesCountY(21),
caseSize(33),
mapSizeX(caseSize * casesCountX),
mapSizeY(caseSize * casesCountY),
fieldSelected(-1),
boardArray(new int[(casesCountX - 1) * (casesCountY - 1)]),
lastUnitSelected(NULL),
lastUnitAction(MapCanvas::NOTHING),
boardMap("resources/tile.png")
{
    font.loadFromFile("resources/playtime.ttf");
    caseValue.setFont(font);
    this->resize(getMapSizeX(), getMapSizeY());
    boardMap.setPosition(getCaseSize() / 2, getCaseSize() / 2);
    this->setBoardArray(boardMapVector);
    mekaEngine = new MekaEngine(*this, casesCountX, casesCountY, caseSize);
}

MapCanvas::~MapCanvas()
{
    delete boardArray;
    delete mekaEngine;
}

void    MapCanvas::setBoardArray(const std::vector<int> &data)
{
    unsigned int i;

    for (i = 0; i < data.size() - 2; i++)
    {
        if (data[i] < 0 || data[i] > 2)
            throw MapParserException::BadData();
        boardArray[i] = data[i];
    }
    if (data[i] < 10 || data[i] > 31 || data[i + 1] < 10 || data[i + 1] > 21)
        throw MapParserException::BadData();
    casesCountX = data[i++];
    casesCountY = data[i];
}

void    MapCanvas::handleLastUnitActions(const int onPosX, const int onPosY)
{
    std::string tmp;

    try
    {
        if (lastUnitAction == MapCanvas::MOVE)
        {
            emit activateMoveButton(true);
            lastUnitAction = MapCanvas::NOTHING;
            mekaEngine->moveUnitToPosition(lastUnitSelected, onPosX, onPosY);
        }
        else if (lastUnitAction == MapCanvas::ATTACK)
        {
            emit activateAttackButton(true);
            lastUnitAction = MapCanvas::NOTHING;
            tmp = "You inflicted ";
            tmp.append(Utils::toStr(mekaEngine->attackUnitAtPosition(lastUnitSelected, onPosX, onPosY)));
            tmp.append(" damages");
            emit info(tmp);
        }
    }
    catch (MekaMovesException::MekaMovesException &e)
    {
        emit badMekaMove(e.what());
    }
}

void    MapCanvas::mousePressEvent(QMouseEvent *ev)
{
    int              curPosX, curPosY;

    getBoardArrayIndexFromMouse(ev->x() - 19, ev->y() - 19, curPosX, curPosY);
    if (curPosX == -1 || curPosY == -1)
    {
        emit badMekaMove("Bad move on map, try again");
        return ;
    }
    fieldSelected = getBoardArrayValue(curPosX, curPosY);
    handleLastUnitActions(curPosX, curPosY);
    mekaEngine->updateArrowPosition(curPosX, curPosY);
    if ((lastUnitSelected = mekaEngine->getUnitAt(curPosX, curPosY)) == NULL)
        emit updateMekaInfosSignal(0, 0, 0, 0);
    else
        emit updateMekaInfosSignal(lastUnitSelected->getLifePoints(), lastUnitSelected->getAttack(),
            lastUnitSelected->getSpeed(), lastUnitSelected->getScope());
    emit updateFieldInfosSignal(fieldSelected);
    if (mekaEngine->checkEndGame() != -1)
        emit teamWins(mekaEngine->checkEndGame());
}

void        MapCanvas::getBoardArrayIndexFromMouse(const int x, const int y, int &retX, int &retY)
{
    retX = -1;
    retY = -1;
    if (x > 0 && y > 0)
    {
        for (int i = 0; i < casesCountX; i++)
        {
            if (x < i * caseSize && x > (i - 1) * caseSize)
                retX = i - 1;
        }
        for (int i = 0; i < casesCountY; i++)
        {
            if (y < i * caseSize && y > (i - 1) * caseSize)
                retY = i - 1;
        }
    }
}

void     MapCanvas::setBoardArrayValue(const int x, const int y, const int value)
{
    boardArray[(casesCountX * y + x) - y] = value;
}

int     MapCanvas::getBoardArrayValue(const int x, const int y) const
{
    return boardArray[(casesCountX * y + x) - y];
}

void MapCanvas::onUpdate()
{
    if (!boardMap.load(sf::Vector2u(getCaseSize(), getCaseSize()), boardArray, (getCasesCountX() - 1), (getCasesCountY() - 1)))
        throw GlobalException("Cannot load tile image!");

    this->clear(sf::Color(222, 184, 135));
    this->draw(boardMap);
    this->draw(mekaEngine->getTileMap());
    this->draw(mekaEngine->getArrowTileMap());
}

void MapCanvas::onInit() { }

int MapCanvas::getCaseSize() const
{
    return caseSize;
}

int MapCanvas::getCasesCountX() const
{

    return casesCountX;
}

void MapCanvas::setCasesCountX(const int casesCountX)
{
    this->casesCountX = casesCountX;
    this->mapSizeX = caseSize * casesCountX;
}

int MapCanvas::getCasesCountY() const
{

    return casesCountY;
}

void MapCanvas::setCasesCountY(const int casesCountY)
{
    this->casesCountY = casesCountY;
    this->mapSizeY = caseSize * casesCountY;
}

int MapCanvas::getMapSizeX() const
{
    return mapSizeX;
}

int MapCanvas::getMapSizeY() const
{
    return mapSizeY;
}

Unit::FieldType MapCanvas::getFieldSelected() const
{
    return static_cast<Unit::FieldType> (fieldSelected);
}

void MapCanvas::setFieldSelected(int fieldSelected)
{
    this->fieldSelected = fieldSelected;
}

int     *MapCanvas::getBoardArray() const
{
    return boardArray;
}

void        MapCanvas::setLastUnitAction(const UnitAction action)
{
    this->lastUnitAction = action;
}

bool        MapCanvas::lastUnitIsFromTeam(const int fromTeam)
{
    if (lastUnitSelected == NULL)
        return false;
    else
        return mekaEngine->isUnitFromTeam(lastUnitSelected, fromTeam);
}

Unit                *MapCanvas::getLastUnitSelected()
{
    return lastUnitSelected;
}

void            MapCanvas::resetUnitsTurnsLimitations()
{
    mekaEngine->resetUnitsTurnsLimitations();
}