
#include "MainWindow.hh"
#include "ui_mainwindow.h"
#include "Menu.hh"

MainWindow::MainWindow(Menu *parent, const std::string &mapFile) :
QMainWindow(parent),
ui(new Ui::MainWindow),
parent(parent),
isUnitSelected(false),
curTeam(0)
{
    ui->setupUi(this);
    frame = new QFrame(this);
    frame->setWindowTitle("Map editor");
    frame->move(0, 0);
    canvas = new MapCanvas(frame, QPoint(0, 30), QSize(360, 360), parseMapFile(mapFile));
    frame->resize(canvas->getMapSizeX() + canvas->getCasesCountX() * 2,
            canvas->getMapSizeY() + canvas->getMapSizeY() * 2);
    parseMapFile(mapFile);
    connectCanvasSignals();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete canvas;
    delete frame;
}

void    MainWindow::connectCanvasSignals()
{
    connect(canvas, SIGNAL(updateFieldInfosSignal(int)), this, SLOT(onUpdateFieldInfos(int)));
    connect(canvas, SIGNAL(updateMekaInfosSignal(int, int, int, int)), this, SLOT(onUpdateMekaInfos(int, int, int, int)));
    connect(canvas, SIGNAL(badMekaMove(const std::string &)), this, SLOT(onAddInfo(const std::string &)));
    connect(canvas, SIGNAL(info(const std::string &)), this, SLOT(onAddInfo(const std::string &)));
    connect(canvas, SIGNAL(activateMoveButton(const bool)), this, SLOT(onActivateMoveButton(const bool)));
    connect(canvas, SIGNAL(activateSpecialButton(const bool)), this, SLOT(onActivateSpecialButton(const bool)));
    connect(canvas, SIGNAL(activateAttackButton(const bool)), this, SLOT(onActivateAttackButton(const bool)));
    connect(canvas, SIGNAL(activateCancelActionButton(const bool)), this, SLOT(onActivateCancelActionButton(const bool)));
    connect(canvas, SIGNAL(teamWins(const int)), this, SLOT(onTeamWins(const int)));
}

std::vector<int>        MainWindow::parseMapFile(const std::string &mapFile)
{
    std::vector<int>    board;
    std::string         coord;
    std::ifstream       myfile(mapFile.c_str());

    if (myfile.is_open())
    {
        while (myfile.good())
        {
            getline(myfile, coord, ':');
            if (!coord.empty() && coord != "\n")
                board.push_back(Utils::toInt(coord));
        }
        myfile.close();
    }
    else
        throw MapParserException::InvalidFile();
    return board;
}

void    MainWindow::closeEvent(QCloseEvent *)
{
    this->destroy();
    parent->show();
}

void        MainWindow::onUpdateFieldInfos(int fieldSelected)
{
    switch (fieldSelected)
    {
        case Unit::MOUTAINS:
            ui->lblScopeBonus->setText("+1");
            ui->lblSpeedBonus->setText("-2");
            ui->lblAttackBonus->setText("+0");
            ui->lblDefenseBonus->setText("+0");
            break;
        case Unit::RIVER:
            ui->lblScopeBonus->setText("+0");
            ui->lblSpeedBonus->setText("-1");
            ui->lblAttackBonus->setText("+0");
            ui->lblDefenseBonus->setText("+1");
            break;
        case Unit::PLAIN:
            ui->lblScopeBonus->setText("+0");
            ui->lblSpeedBonus->setText("+0");
            ui->lblAttackBonus->setText("+1");
            ui->lblDefenseBonus->setText("+1");
            break;
    }
}

void        MainWindow::onUpdateMekaInfos(int lifePoints, int attack, int speed, int scope)
{
    if (lifePoints == 0)
        isUnitSelected = false;
    else
        isUnitSelected = true;
    ui->lblLifePt->setText(QString::fromStdString(Utils::toStr(lifePoints)));
    ui->lblAttack->setText(QString::fromStdString(Utils::toStr(attack)));
    ui->lblSpeed->setText(QString::fromStdString(Utils::toStr(speed)));
    ui->lblScope->setText(QString::fromStdString(Utils::toStr(scope)));
}

void        MainWindow::onSwitchTeamTurn(int team)
{
    curTeam = team;
    if (curTeam == 0)
        ui->lblTeamTurn->setText("<font color='blue'>Blue team turn</font>");
    else
        ui->lblTeamTurn->setText("<font color='red'>Red team turn</font>");
}

void        MainWindow::onAddInfo(const std::string &str)
{
    if (curTeam == 0)
        ui->tbInfoBox->setTextColor(QColor("blue"));
    else
        ui->tbInfoBox->setTextColor(QColor("red"));
    ui->tbInfoBox->append(QString::fromStdString(str));
}

void        MainWindow::onActivateMoveButton(const bool isActivated)
{
    ui->btMoveUnit->setEnabled(isActivated);
}

void        MainWindow::onActivateAttackButton(const bool isActivated)
{
    ui->btAttackUnit->setEnabled(isActivated);
}

void        MainWindow::onActivateSpecialButton(const bool isActivated)
{
    ui->btActivateSpecial->setEnabled(isActivated);
}

void        MainWindow::onActivateCancelActionButton(const bool isActivated)
{
    ui->btCancelAction->setEnabled(isActivated);
}

void        MainWindow::onTeamWins(const int teamWinner)
{
    if (teamWinner == 0)
        QMessageBox::information(this, "Game Ends", "Blue team wins!!");
    else
        QMessageBox::information(this, "Game Ends", "Red team wins!!");
    this->destroy();
    parent->show();
}

void MainWindow::on_btMoveUnit_clicked()
{
    if (isUnitSelected)
    {
        if (!canvas->getLastUnitSelected()->getCanMove())
        {
            onAddInfo("This unit has already moved for this turn");
            return ;
        }
        if (canvas->lastUnitIsFromTeam(curTeam))
        {
            ui->btAttackUnit->setEnabled(true);
            ui->btMoveUnit->setEnabled(false);
            canvas->setLastUnitAction(MapCanvas::MOVE);
        }
        else
            onAddInfo("This unit is not from your team!");
    }
    else
        onAddInfo("No unit selected");
}

void MainWindow::on_btAttackUnit_clicked()
{
    if (isUnitSelected)
    {
        if (!canvas->getLastUnitSelected()->getCanAttack())
        {
            onAddInfo("This unit has already attacked for this turn");
            return ;
        }
        if (canvas->lastUnitIsFromTeam(curTeam))
        {
            ui->btMoveUnit->setEnabled(true);
            ui->btAttackUnit->setEnabled(false);
            canvas->setLastUnitAction(MapCanvas::ATTACK);
        }
        else
            onAddInfo("This unit is not from your team!");
    }
    else
        onAddInfo("No unit selected");
}

void MainWindow::on_btFinishTurn_clicked()
{
    if (curTeam == 0)
        onSwitchTeamTurn(1);
    else
        onSwitchTeamTurn(0);
    canvas->resetUnitsTurnsLimitations();
}

void MainWindow::on_btActivateSpecial_clicked()
{
    Unit        *unit = canvas->getLastUnitSelected();

    if (unit != NULL)
    {
        if (unit->isSpecialActive())
        {
            onAddInfo("This unit has already activated his special");
            return ;
        }
        if (canvas->lastUnitIsFromTeam(curTeam))
        {
            if (unit->getType() == Unit::TANK)
                onAddInfo("Tank special activated!");
            else if (unit->getType() == Unit::MISSILE_LAUNCHER)
                onAddInfo("Missile launcher special activated!");
            else if (unit->getType() == Unit::BERSERK)
                onAddInfo("Berserk special activated!");
            unit->enableSpecial();
        }
        else
            onAddInfo("This unit is not from your team!");
    }
    else
        onAddInfo("No unit selected!");

}

void MainWindow::on_btCancelAction_clicked()
{
    ui->btMoveUnit->setEnabled(true);
    ui->btAttackUnit->setEnabled(true);
    canvas->setLastUnitAction(MapCanvas::NOTHING);
}

void MainWindow::on_actionQuit_triggered()
{
    exit(0);
}
