/* 
 * File:   MekaEngine.cpp
 * Author: godard_b
 * 
 * Created on July 26, 2013, 2:27 AM
 */

#include "MekaEngine.hh"
#include "TileMaper.hh"
#include "MapCanvas.hh"

MekaEngine::MekaEngine(MapCanvas &parent, const int mekaMapX, const int mekaMapY, const int mekaCaseSize) :
parent(parent),
mekaArraySize((mekaMapX - 1) * (mekaMapY - 1)),
mekaArray(new int[mekaArraySize]),
mekaMapX(mekaMapX),
mekaMapY(mekaMapY),
mekaCaseSize(mekaCaseSize),
mekaMap("resources/sprites.png"),
arrowMap("resources/arrow.png")
{
    mekaMap.setPosition(mekaCaseSize / 2, mekaCaseSize / 2);
    arrowMap.setPosition(mekaCaseSize / 2, mekaCaseSize / 2);
    clearMapArray();
    initTeams();
}

MekaEngine::~MekaEngine()
{
    delete mekaArray;
}

void    MekaEngine::clearMapArray()
{
    for (int i = 0; i < mekaArraySize; i++)
        mekaArray[i] = static_cast<int> (NONE);
}

void    MekaEngine::initTeams()
{
    teams[0].addUnit(new Tank(1, 0, static_cast<Unit::FieldType> (parent.getBoardArrayValue(1, 0))));
    teams[0].addUnit(new Tank(1, mekaMapY / 3, static_cast<Unit::FieldType> (parent.getBoardArrayValue(1, mekaMapY / 3))));
    teams[0].addUnit(new Tank(1, (mekaMapY / 3) * 2, static_cast<Unit::FieldType> (parent.getBoardArrayValue(1, (mekaMapY / 3) * 2))));
    teams[0].addUnit(new MissileLauncher(0, mekaMapY / 4, static_cast<Unit::FieldType> (parent.getBoardArrayValue(0, mekaMapY / 4))));
    teams[0].addUnit(new Berserk(0, (mekaMapY / 4) * 3, static_cast<Unit::FieldType> (parent.getBoardArrayValue(0, (mekaMapY / 4) * 3))));

    teams[1].addUnit(new Tank(mekaMapX - 3, 0, static_cast<Unit::FieldType> (parent.getBoardArrayValue(mekaMapX - 3, 0))));
    teams[1].addUnit(new Tank(mekaMapX - 3, mekaMapY / 3, static_cast<Unit::FieldType> (parent.getBoardArrayValue(mekaMapX - 3, mekaMapY / 3))));
    teams[1].addUnit(new Tank(mekaMapX - 3, (mekaMapY / 3) * 2, static_cast<Unit::FieldType> (parent.getBoardArrayValue(mekaMapX - 3, (mekaMapY / 3) * 2))));
    teams[1].addUnit(new MissileLauncher(mekaMapX - 2, mekaMapY / 4, static_cast<Unit::FieldType> (parent.getBoardArrayValue(mekaMapX - 2, mekaMapY / 4))));
    teams[1].addUnit(new Berserk(mekaMapX - 2, (mekaMapY / 4) * 3, static_cast<Unit::FieldType> (parent.getBoardArrayValue(mekaMapX - 2, (mekaMapY / 4) * 3))));
}

void     MekaEngine::setMekaArrayValue(const int x, const int y, const int value)
{
    mekaArray[(mekaMapX * y + x) - y] = value;
}

Unit            *MekaEngine::getUnitAt(const int x, const int y)
{
    std::vector<Unit *> units;

    for (int i = 0; i < 2; i++)
    {
        units = teams[i].getUnits();
        for (std::vector<Unit *>::iterator it = units.begin(); it != units.end(); ++it)
        {
            if ((*it)->getX() == x && (*it)->getY() == y)

                return (*it);
        }
    }
    return NULL;
}

void    MekaEngine::updateArrowPosition(const int onPosX, const int onPosY)
{
    int arrowArray[mekaArraySize];

    //TODO arrow animation
    if (onPosX == -1 || onPosY == -1)
        return ;
    for (int i = 0; i < mekaArraySize; i++)
        arrowArray[i] = ARROW_NOTHING;
    if (parent.getFieldSelected() != -1)
    {
        if (onPosY > 0)
            arrowArray[(mekaMapX * (onPosY - 1) + onPosX) - onPosY + 1] = ARROW_DOWN1;
        else
            arrowArray[(mekaMapX * (onPosY + 1) + onPosX) - onPosY - 1] = ARROW_UP1;
    }
    if (!arrowMap.load(sf::Vector2u(mekaCaseSize, mekaCaseSize), arrowArray, (mekaMapX - 1), (mekaMapY - 1)))
        throw GlobalException("Cannot load tile image!");
}

TileMaper       MekaEngine::getArrowTileMap()
{
    return arrowMap;
}

TileMaper       MekaEngine::getTileMap()
{
    std::vector<Unit *> units;

    clearMapArray();
    //i = 0 : blue team, i = 1: red team
    for (int i = 0; i < 2; i++)
    {
        units = teams[i].getUnits();
        for (unsigned int j = 0; j < units.size(); j++)
        {
            switch (units[j]->getType())
            {
                case Unit::TANK:
                    if (i == 0)
                        setMekaArrayValue(units[j]->getX(),
                            units[j]->getY(), TANK_BLUE);
                    else
                        setMekaArrayValue(units[j]->getX(),
                            units[j]->getY(), TANK_RED);
                    break;
                case Unit::MISSILE_LAUNCHER:
                    if (i == 0)
                        setMekaArrayValue(units[j]->getX(),
                            units[j]->getY(), MISSILE_LAUNCHER_BLUE);
                    else
                        setMekaArrayValue(units[j]->getX(),
                            units[j]->getY(), MISSILE_LAUNCHER_RED);
                    break;
                case Unit::BERSERK:
                    if (i == 0)
                        setMekaArrayValue(units[j]->getX(),
                            units[j]->getY(), BERSERK_BLUE);
                    else
                        setMekaArrayValue(units[j]->getX(),
                            units[j]->getY(), BERSERK_RED);
                    break;
            }

        }
    }
    if (!mekaMap.load(sf::Vector2u(mekaCaseSize, mekaCaseSize), mekaArray, (mekaMapX - 1), (mekaMapY - 1)))
        throw GlobalException("Cannot load tile image!");

    return mekaMap;
}

bool        MekaEngine::isUnitFromTeam(Unit *unit, const int fromTeam)
{
    std::vector<Unit *> units;

    for (int i = 0; i < 2; i++)
    {
        units = teams[i].getUnits();
        for (std::vector<Unit *>::iterator it = units.begin(); it != units.end(); ++it)
        {
            if ((*it) == unit && i == fromTeam)
                return true;
        }
    }
    return false;
}

void            MekaEngine::removeUnit(Unit *unit)
{
    std::vector<Unit *> units;

    for (int i = 0; i < 2; i++)
    {
        units = teams[i].getUnits();
        for (std::vector<Unit *>::iterator it = units.begin(); it != units.end(); ++it)
        {
            if ((*it) == unit)
            {
                teams[i].removeUnit(*it);
                break;
            }
        }
    }
}

void            MekaEngine::resetUnitsTurnsLimitations()
{
    std::vector<Unit *> units;

    for (int i = 0; i < 2; i++)
    {
        units = teams[i].getUnits();
        for (std::vector<Unit *>::iterator it = units.begin(); it != units.end(); ++it)
        {
            (*it)->setCanMove(true);
            (*it)->setCanAttack(true);
            if ((*it)->isSpecialActive())
                (*it)->disableSpecial();
        }
    }
}

bool    MekaEngine::posIsInRange(const int originX, const int originY, const int toX, const int toY, const int range)
{
    if ((toX - originX <= range && originX - toX <= range) && (toY - originY <= range && originY - toY <= range))
        return true;
    return false;
}

void    MekaEngine::moveUnitToPosition(Unit *unit, const int xPos, const int yPos)
{
    int                         casesCountMoveAllowed = 0;
    Unit::FieldType fieldType = parent.getFieldSelected();

    if (getUnitAt(xPos, yPos) != NULL)
        throw MekaMovesException::BadMoveException();
    if (!unit->getCanMove())
        throw MekaMovesException::AlreadyMoveException();
    casesCountMoveAllowed = unit->getSpeed();
    if (fieldType == Unit::MOUTAINS)
        casesCountMoveAllowed -= 2;
    else if (fieldType == Unit::RIVER)
        casesCountMoveAllowed--;
    if (posIsInRange(unit->getX(), unit->getY(), xPos, yPos, casesCountMoveAllowed))
    {
        unit->setPosX(xPos);
        unit->setPosY(yPos);
        unit->setOnFieldType(static_cast<Unit::FieldType> (fieldType));
        unit->setCanMove(false);
    }
    else
        throw MekaMovesException::FarMoveException();
}

int    MekaEngine::attackUnitAtPosition(Unit *attacker, const int onX, const int onY)
{
    int         damages = 0;
    int         range = 0;
    Unit        *target;

    if ((target = getUnitAt(onX, onY)) == NULL)
        throw MekaMovesException::NoTargetFoundException();
    if (!attacker->getCanAttack())
        throw MekaMovesException::AlreadyAttackException();
    range = attacker->getScope();
    if (attacker->getOnFieldType() == Unit::MOUTAINS)
        range++;
    if (!posIsInRange(attacker->getX(), attacker->getY(), onX, onY, range))
        throw MekaMovesException::AttackOutOfScopeException();
    damages = attacker->getAttack();
    if (attacker->getOnFieldType() == Unit::PLAIN)
        damages++;
    if (target->getOnFieldType() == Unit::RIVER || target->getOnFieldType() == Unit::PLAIN)
        damages--;
    target->setLifePoint(target->getLifePoints() - damages);
    if (target->getLifePoints() <= 0)
        removeUnit(target);
    attacker->setCanAttack(false);
    return damages;
}

int    MekaEngine::checkEndGame()
{
    if (teams[0].getUnits().size() <= 0)
        return 1;
    else if (teams[1].getUnits().size() <= 0)
        return 0;
    return -1;
}