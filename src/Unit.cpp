/* 
 * File:   Unit.cpp
 * Author: godard_b
 * 
 * Created on July 23, 2013, 11:44 AM
 */

#include "Unit.hh"

Unit::Unit() :
canMove(true),
canAttack(true) { }

Unit::~Unit() { }

int         Unit::getX() const
{
    return x;
}

int         Unit::getY() const
{
    return y;
}

void        Unit::setPosX(const int x)
{
    this->x = x;
}

void        Unit::setPosY(const int y)
{
    this->y = y;
}

void        Unit::setScope(const int scope)
{
    this->scope = scope;
}

int         Unit::getScope() const
{
    return scope;
}

void        Unit::setAttack(const int attack)
{
    this->attack = attack;
}

int         Unit::getAttack() const
{
    return attack;
}

void        Unit::setSpeed(const int speed)
{
    this->speed = speed;
}

int         Unit::getSpeed() const
{
    return speed;
}

int         Unit::getLifePoints() const
{
    return lifePoints;
}

void        Unit::setLifePoint(const int lifePoints)
{
    this->lifePoints = lifePoints;
}

Unit::FieldType   Unit::getOnFieldType() const
{
    return onFieldType;
}

void        Unit::setOnFieldType(const Unit::FieldType type)
{
    this->onFieldType = type;
}

bool        Unit::getCanMove() const
{
    return canMove;
}

void        Unit::setCanMove(const bool canMove)
{
    this->canMove = canMove;
}

bool        Unit::getCanAttack() const
{
    return canAttack;
}

void        Unit::setCanAttack(const bool canAttack)
{
    this->canAttack = canAttack;
}