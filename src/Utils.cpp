/* 
 * File:   Utils.cpp
 * Author: godard_b
 * 
 * Created on July 11, 2013, 8:23 PM
 */

#include "Utils.hh"

std::string Utils::toStr(const int nbr)
{
    std::ostringstream oss;

    oss << nbr;
    return oss.str();
}

int         Utils::toInt(const std::string &nb)
{
    int numb;
    std::istringstream buffer(nb);

    if (!(buffer >> numb))
        throw MapParserException::BadData();
    return numb;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;

    while (std::getline(ss, item, delim))
        elems.push_back(item);
    return elems;
}