/* 
 * File:   MissileLauncher.cpp
 * Author: godard_b
 * 
 * Created on July 23, 2013, 3:36 PM
 */

#include "MissileLauncher.hh"

MissileLauncher::MissileLauncher(const int x, const int y, const Unit::FieldType type)
{
    this->setPosX(x);
    this->setPosY(y);
    this->setLifePoint(5);
    this->setAttack(6);
    this->setSpeed(6);
    this->setScope(5);
    this->setOnFieldType(type);
    specialActive = false;
}

void        MissileLauncher::enableSpecial()
{
    specialActive  = true;
}

void        MissileLauncher::disableSpecial()
{
    specialActive = false;
}

bool        MissileLauncher::isSpecialActive()
{
    return specialActive;
}

Unit::UnitType    MissileLauncher::getType()
{
    return Unit::MISSILE_LAUNCHER;
}