/* 
 * File:   Tank.cpp
 * Author: godard_b
 * 
 * Created on July 23, 2013, 3:35 PM
 */

#include "Tank.hh"

Tank::Tank(const int x, const int y, const Unit::FieldType type)
{
    this->setPosX(x);
    this->setPosY(y);
    this->setLifePoint(10);
    this->setAttack(5);
    this->setSpeed(7);
    this->setScope(3);
    this->setOnFieldType(type);
    specialActive = false;
}

void        Tank::enableSpecial()
{
    specialActive = true;
    setAttack(getAttack() * 2);
}

void        Tank::disableSpecial()
{
    if (specialActive)
    {
        specialActive = false;
        setAttack(getAttack() / 2);
    }
}

bool    Tank::isSpecialActive()
{
    return specialActive;
}

Unit::UnitType    Tank::getType()
{
    return Unit::TANK;
}