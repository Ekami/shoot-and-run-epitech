#include	"Exception.hh"

GlobalException::GlobalException(const std::string &error) : _errormsg(error) { }

GlobalException::~GlobalException() throw () { }

const char	*GlobalException::what() const throw ()
{
    return (_errormsg.c_str());
}
