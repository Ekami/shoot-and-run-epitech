/* 
 * File:   MapCanvas.cpp
 * Author: godard_b
 * 
 * Created on July 21, 2013, 7:26 PM
 */

#include <sstream>
#include "CreatorCanvas.hh"

CreatorCanvas::CreatorCanvas(QWidget* Parent, const QPoint& Position, const QSize& Size)
: QSFMLCanvas(Parent, Position, Size, FRAME_TIME),
casesCountX(31),
casesCountY(21),
caseSize(33),
mapSizeX(caseSize * casesCountX),
mapSizeY(caseSize * casesCountY),
curPosX(-1),
curPosY(-1),
fieldSelected(2),
boardArray(new int[(casesCountX - 1) * (casesCountY - 1)]),
boardMap("resources/tile.png")
{
    font.loadFromFile("resources/playtime.ttf");
    caseValue.setFont(font);
    this->resize(mapSizeX, mapSizeY);
    boardMap.setPosition(caseSize / 2, caseSize / 2);
}

CreatorCanvas::~CreatorCanvas()
{
    delete boardArray;
}

void        CreatorCanvas::getBoardArrayIndexFromMouse(const int x, const int y, int &retX, int &retY)
{
    retX = -1;
    retY = -1;
    if (x > 0 && y > 0)
    {
        for (int i = 0; i < casesCountX; i++)
        {
            if (x < i * caseSize && x > (i - 1) * caseSize)
                retX = i - 1;
        }
        for (int i = 0; i < casesCountY; i++)
        {
            if (y < i * caseSize && y > (i - 1) * caseSize)
                retY = i - 1;
        }
    }
}

void     CreatorCanvas::setBoardArrayValue(const int x, const int y, const int value)
{
    boardArray[(casesCountX * y + x) - y] = value;
}

void    CreatorCanvas::mousePressEvent(QMouseEvent *ev)
{
    getBoardArrayIndexFromMouse(ev->x() - 19, ev->y() - 19, curPosX, curPosY);
    if (curPosX != -1 && curPosY != -1)
        setBoardArrayValue(curPosX, curPosY, fieldSelected);
}

void    CreatorCanvas::mouseMoveEvent(QMouseEvent *ev)
{
    getBoardArrayIndexFromMouse(ev->x() - 19, ev->y() - 19, curPosX, curPosY);
    if (curPosX != -1 && curPosY != -1)
        setBoardArrayValue(curPosX, curPosY, fieldSelected);
}

void CreatorCanvas::onUpdate()
{
    if (!boardMap.load(sf::Vector2u(caseSize, caseSize), boardArray, (casesCountX - 1), (casesCountY - 1)))
        throw GlobalException("Cannot load tile image!");

    this->clear(sf::Color(222, 184, 135));
    this->draw(boardMap);
}

void    CreatorCanvas::clearMap()
{
    for (int i  = 0; i < (casesCountX - 1) * (casesCountY - 1); i++)
        boardArray[i] = 2;
}

void CreatorCanvas::onInit()
{
    clearMap();
}

int CreatorCanvas::getCaseSize() const
{

    return caseSize;
}

int CreatorCanvas::getCasesCountX() const
{

    return casesCountX;
}

void CreatorCanvas::setCasesCountX(const int casesCountX)
{

    this->casesCountX = casesCountX;
    this->mapSizeX = caseSize * casesCountX;
    clearMap();
}

int CreatorCanvas::getCasesCountY() const
{

    return casesCountY;
}

void CreatorCanvas::setCasesCountY(const int casesCountY)
{

    this->casesCountY = casesCountY;
    this->mapSizeY = caseSize * casesCountY;
    clearMap();
}

int CreatorCanvas::getMapSizeX() const
{

    return mapSizeX;
}

int CreatorCanvas::getMapSizeY() const
{
    return mapSizeY;
}

int CreatorCanvas::getFieldSelected() const
{
    return fieldSelected;
}

void CreatorCanvas::setFieldSelected(int fieldSelected)
{
    this->fieldSelected = fieldSelected;
}

int     *CreatorCanvas::getBoardArray() const
{
    return boardArray;
}