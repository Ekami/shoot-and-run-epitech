#include "MapCreatorMW.hh"
#include "ui_Mapcreatormw.h"

MapCreatorMW::MapCreatorMW(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MapCreatorMW),
parent(parent)
{
    ui->setupUi(this);
    frame = new QFrame(this);
    frame->setWindowTitle("Map editor");
    this->setWindowTitle("Map Editor");
    frame->move(0, 0);
    canvas = new CreatorCanvas(frame, QPoint(0, 30), QSize(360, 360));
    frame->resize(canvas->getMapSizeX() + canvas->getCasesCountX() * 2,
            canvas->getMapSizeY() + canvas->getMapSizeY() * 2);
}

MapCreatorMW::~MapCreatorMW()
{
    delete ui;
}

void        MapCreatorMW::createMapFile(const std::string &mapFilePath)
{
    std::ofstream myfile;
    int           *boardArray = canvas->getBoardArray();

    myfile.open (mapFilePath.c_str());
    for (int i = 0; i < (canvas->getCasesCountX() - 1) *
            (canvas->getCasesCountY() - 1); i++)
    {
        myfile << boardArray[i];
        myfile << ":";
    }
    myfile << canvas->getCasesCountX();
    myfile << ":";
    myfile << canvas->getCasesCountY();
    myfile << ":";
    myfile.close();
}

void MapCreatorMW::on_btPickMoutain_clicked()
{
    canvas->setFieldSelected(static_cast<int> (MOUTAINS));
    ui->btPickMoutain->setEnabled(false);
    ui->btPickPlain->setEnabled(true);
    ui->btPickWater->setEnabled(true);
}

void MapCreatorMW::on_btPickWater_clicked()
{
    canvas->setFieldSelected(static_cast<int> (WATER));
    ui->btPickWater->setEnabled(false);
    ui->btPickMoutain->setEnabled(true);
    ui->btPickPlain->setEnabled(true);
}

void MapCreatorMW::on_btPickPlain_clicked()
{
    canvas->setFieldSelected(static_cast<int> (PLAIN));
    ui->btPickPlain->setEnabled(false);
    ui->btPickMoutain->setEnabled(true);
    ui->btPickWater->setEnabled(true);
}

void MapCreatorMW::on_sbMapWidth_valueChanged(int arg1)
{
    canvas->setCasesCountX(arg1);
}

void MapCreatorMW::on_sbMapHeight_valueChanged(int arg1)
{
    canvas->setCasesCountY(arg1);
}

void MapCreatorMW::on_pushButton_clicked()
{
    canvas->clearMap();
}

void MapCreatorMW::on_btSaveMap_clicked()
{
    QString filePath = QFileDialog::getSaveFileName(this, "Save map", ".");

    createMapFile(filePath.toStdString());
}

void    MapCreatorMW::closeEvent(QCloseEvent *)
{
    this->hide();
    parent->show();
}