/* 
 * File:   Player.cpp
 * Author: godard_b
 * 
 * Created on June 18, 2013, 1:20 AM
 */

#include "Player.hh"

int Player::getScore() const
{
    return score;
}

void        Player::setScore(const int score)
{
    this->score = score;
}